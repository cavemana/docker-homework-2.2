# Docker homework 2.2

## Objective 📋

We have two nginx services. Backend serves now the right static page and happy developer has created the frontend server instance with proper upstream configuration. To make communication between containers possible he has defined the "ports" instruction in docker-compose.yaml, but he hadn't expected the actual result of the pipline.

## Solution 👌

### You've need to investigate: 
- nginx configuration files
- Dockerfiles
- docker-compose.yaml 

__IMPORTANT!__ The solution, should be based only on proper docker-compose.override.yaml configuration!

### To make the things work properly, please follow the manual below:

- Fork the project 
- Create proper docker-compose.override.yaml
- Commit and push the changes
- If the pipeline succeeds, you have completed the task
  
## Author 👀
Glukhov Aleksandr / DT IT DevOps School
